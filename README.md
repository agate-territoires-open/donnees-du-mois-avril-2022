# Données du mois

## Présentation
Les données du mois sont produites par l'[Agence Alpine des Territoires](https://agate-territoires.fr/). Elles permettent de mettre en valeur des données, thématiques ou sujets qui peuvent être abordés dans les missions de l'agence. Il s'agit de données en Open Data, travaillées par l'équipe "géomatique" afin de proposer différents modes de représentation.

## Contact
Pour toute question / remarque / suggestion, n'hésitez pas à nous contacter : geomatique@agate-territoires.fr

## Licence
Nous déposerons ici les projets pouvant être partagés sous licence libre.
